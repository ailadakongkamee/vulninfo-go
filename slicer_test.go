package vulninfo

import (
	"github.com/stretchr/testify/require"
	"strings"
	"testing"
)

func TestTracker(t *testing.T) {
	ret, err := VulnInfo.CommonAncestors("cwe-39", "cwe-32", PARENT_OF)
	if err != nil {
		t.Fail()
	}

	if ret[0].First.Node != "cwe-22" {
		t.Fatalf("ancestor should be 22")
	}

	// cwe-39 -> cwe-36 -> cwe-22
	if ret[0].First.Level != 2 {
		t.Fatalf("path length should be 2")
	}

	// cwe-32 -> cwe-23 -> cwe-22
	if ret[0].Second.Level != 2 {
		t.Fatalf("path length should be 2")
	}

	ret, _ = VulnInfo.CommonAncestors("cwe-1002", "cwe-39", PARENT_OF)
	if err != nil {
		t.Fail()
	}
	if len(ret) != 0 {
		t.Fail()
	}
}

func TestSlicer(t *testing.T) {
	cwe78, ok := VulnInfo.nodes["cwe-22"]
	if !ok {
		t.Fail()
	}

	slice, _, _ := VulnInfo.Slice(cwe78.Identifier, FORWARD, PARENT_OF)

	expect := []string{
		"cwe-23:parent_of:cwe-32",
		"cwe-23:parent_of:cwe-27",
		"cwe-36:parent_of:cwe-38",
		"cwe-23:parent_of:cwe-28",
		"cwe-23:parent_of:cwe-29",
		"cwe-23:parent_of:cwe-33",
		"cwe-22:parent_of:cwe-36",
		"cwe-36:parent_of:cwe-39",
		"cwe-23:parent_of:cwe-31",
		"cwe-36:parent_of:cwe-37",
		"cwe-36:parent_of:cwe-40",
		"cwe-23:parent_of:cwe-24",
		"cwe-23:parent_of:cwe-25",
		"cwe-23:parent_of:cwe-26",
		"cwe-23:parent_of:cwe-30",
		"cwe-23:parent_of:cwe-34",
		"cwe-23:parent_of:cwe-35",
		"cwe-22:parent_of:cwe-23",
		"cwe-23:parent_of:cwe-32",
	}

	for _, rule := range expect {
		items := strings.Split(rule, ":")

		src := items[0]
		//rel := items[1]
		dst := items[2]

		_, err := slice.NodeById(src)
		require.NoError(t, err, "could not find node")
		_, err = slice.NodeById(dst)
		require.NoError(t, err, "could not find node")
		_, err = slice.Relation(rule)
		require.NoError(t, err, "could not find relation")
		_, err = slice.Relation(rule)
		require.NoError(t, err, "could not find relation")
		_, err = slice.OutRelations(src)
		require.NoError(t, err, "could not find out relation")
		_, err = slice.InRelations(dst)
		require.NoError(t, err, "could not find in relation")
	}
}
