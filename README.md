# vulninfo-go

CWE vulnerability information go bindings.

This project exposes a partial collection from the 
[Common Weakness Enumeration Dictionary](https://cwe.mitre.org/data/index.html) as a go module.

## Installation

vulninfo-go can be installed by running the command below.

```bash
go get gitlab.com/gitlab-org/vulnerability-research/foss/vulninfo/vulninfo-go
```

Afterwards, vulninfo is ready to be used as illustrated below.

``` go 
package main

import (
    "fmt"
    "gitlab.com/gitlab-org/vulnerability-research/foss/vulninfo/vulninfo-go"
)

func main() {
    fmt.Println("test")

    cwe78, _ := vulninfo.VulnInfo.Nodes["cwe-78"]

    fmt.Println(cwe78.Title)
    fmt.Println(cwe78.Description)
}
```

## Usage 


At the moment we only extract the following relationship types between CWEs:

``` bash
UNKNOWN     
CHILD_OF    
CAN_PRECEDE 
CAN_ALSO_BE 
MAPS_TO     
PARENT_OF   
PEER_OF     
REQUIRES    
STARTS_WITH 
FOLLOWED_BY
```

For CWEs, we extract `Identifier`, `Title` and `Description`.

```
Identifier:  "cwe-1004",
Title:       `Sensitive Cookie Without 'HttpOnly' Flag`,
Description: `The software uses a cookie to store sensitive information, but the cookie is not marked with the HttpOnly flag.`,
```

The CWE information is available by means of the `VulnInfo` object which
provides access to the CWE ontology. As illustrated in the code snippet below,
`VulnInfo` provides access to CWEs and their relations (e.g., `child_of`,
`parent_of`, etc.).


``` golang
type Ontology struct {
	Nodes        map[string]*VulnInfoNode
	Relations    map[string]Relation
	InRelations  map[string][]string
	OutRelations map[string][]string
}
```

In the code snippet below, you can see how you can easily access CWE
information by means of the `Nodes` map that maps CWE identifiers to the
corresponding CWE information. In the code snippet below, we are just printing
out the CWE title and description fields.

``` golang
cwe78, ok := VulnInfo.Nodes["cwe-78"]
if !ok {
	t.Fail()
}

fmt.Println(cwe78.Title)
fmt.Println(cwe78.Description)
```

Another functionality provided by vulninfo-go is the capability of slicing the
CWE ontology. This is useful if you only want to extract a subset of CWEs based
on their relation.

The code snippet below extracts all the nodes that are reachable by `cwe-78` by
transitively following forward edges and which are in a `parent_of`
relationship.

``` golang
slice, _, _ := VulnInfo.Slice("cwe-78", FORWARD, PARENT_OF)
```

In case you would like to compute whether two given CWEs have a common ancestor
and what the distance to the ancestor is, you can use the code snippet below:

``` golang
ret, err := VulnInfo.CommonAncestors("cwe-39", "cwe-32", PARENT_OF)
```

This function searches for the common ancestors for both `cwe-39` and `cwe-32`
and returns a sorted array of ancestor (sorted by distance from closed to
farthest). 

## License 

The data in this repository is derived from MITRE Common Weakness Enumerations
(CWE).  The MITRE Corporation grants non-exclusive, royalty-free license to use
CWE for research, development, and commercial purposes. For more information,
please have a look at Terms.md or https://cwe.mitre.org/about/termsofuse.html.

The code itself is licensed used under the [MIT license](LICENSE.txt).

