## vulninfo-go

## [1.5.0] - 2021-09-19

- Ontology update (!5)

## [1.4.0] - 2021-09-19

- Ontology update (!4)

## [1.3.0] - 2021-09-07

- CWE ID formatting (!3)

## [1.2.0] - 2021-09-07

- CWE search function (!2)

## [1.1.0] - 2021-09-07

- Improved interface

## [1.0.0] - 2021-06-16

- Initial release
